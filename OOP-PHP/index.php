<?php
    require ('animal.php');

    $sheep = new Animal("shaun");

    echo $sheep->name = "Shaun <br>";
    echo $sheep->legs = "2 <br>";
    echo $sheep->cold_blooded = "false <br>";

    $sungokong = new Ape("kera sakti");
    echo $sungokong->yell(); // "Auooo"   

    $kodok = new Frog("buduk");
    echo $kodok->jump() ; // "hop hop"
?>