<?php
function ubah_huruf($string){
    $temp;
    $nextString;
    echo "$string become ";
    for ($i=0; $i <= strlen($string)-1; $i++) { 
        $temp  = $string[$i];
        $nextString = ++$temp;
        echo $nextString;
    }
    echo "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>