<!DOCTYPE html>
<html>
<body>

<h1>Buat Account baru!</h1>
<h4>Sign Up Form</h4>

<form action = "/welcome" method="POST">
    @csrf
    <label for="fname">First name:</label><br><br>
 	<input type="text" id="fname" name="fname"><br><br>
 	<label for="lname">Last name:</label><br><br>
 	<input type="text" id="lname" name="lname"><br>


<p>Gender:</p>

	<input type="radio" id="male" name="gender" value="male">
	<label for="male">Male</label><br>
	<input type="radio" id="female" name="gender" value="female">
	<label for="female">Female</label><br>
	<input type="radio" id="other" name="gender" value="other">
	<label for="other">Other</label>


<p>Nationality:</p>

	<select id="nationality" name="nationality">
		<option value="Indonesia">Indonesian</option>
		<option value="Singapore">Singaporean</option>
		<option value="Malaysia">Malaysian</option>
		<option value="Australia">Australian</option>
	</select>


<p>Language Spoken:</p>

	<input type="checkbox" id="Bahasa" name="language" value="Bahasa">
	<label for="Bahasa">Bahasa Indonesia</label><br>
	<input type="checkbox" id="English" name="language" value="English">
	<label for="English">English</label><br>
	<input type="checkbox" id="other" name="language" value="other">
	<label for="other">Other</label>


<p>Bio:</p>

    <textarea name="bio" rows="10" cols="30"></textarea><br>
    <input type="submit" value="Sign Up">
    
</form>

</body>
</html>